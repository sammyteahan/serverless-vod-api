"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = void 0;
function response(statusCode, body) {
    return {
        headers: { 'Access-Control-Allow-Origin': '*' },
        statusCode,
        body: JSON.stringify(body),
    };
}
exports.default = response;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVzcG9uc2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi9zcmMvcmVzcG9uc2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBUUEsU0FBUyxRQUFRLENBQUMsVUFBa0IsRUFBRSxJQUFTO0lBQzdDLE9BQU87UUFDTCxPQUFPLEVBQUUsRUFBRSw2QkFBNkIsRUFBRSxHQUFHLEVBQUU7UUFDL0MsVUFBVTtRQUNWLElBQUksRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQztLQUMzQixDQUFDO0FBQ0osQ0FBQztBQUVvQiwyQkFBTyJ9