"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.show = exports.index = void 0;
const s3_1 = require("./s3");
const response_1 = __importDefault(require("./response"));
const index = async () => {
    const vods = await (0, s3_1.listVods)();
    return (0, response_1.default)(200, {
        status: 'success',
        vods: vods,
    });
};
exports.index = index;
const show = async (event) => {
    const objectKey = event.pathParameters ? event.pathParameters.objectKey : null;
    if (!objectKey) {
        return (0, response_1.default)(400, {
            status: 'error',
            message: 'Please include an object key',
        });
    }
    const vod = await (0, s3_1.getVod)({ key: objectKey });
    return (0, response_1.default)(200, {
        status: 'success',
        vod,
    });
};
exports.show = show;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidm9kTWFuYWdlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uL3NyYy92b2RNYW5hZ2VyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUVBLDZCQUF3QztBQUN4QywwREFBbUQ7QUFFbkQsTUFBTSxLQUFLLEdBQUcsS0FBSyxJQUEwQixFQUFFO0lBQzdDLE1BQU0sSUFBSSxHQUFHLE1BQU0sSUFBQSxhQUFRLEdBQUUsQ0FBQztJQUU5QixPQUFPLElBQUEsa0JBQVEsRUFBQyxHQUFHLEVBQUU7UUFDbkIsTUFBTSxFQUFFLFNBQVM7UUFDakIsSUFBSSxFQUFFLElBQUk7S0FDWCxDQUFDLENBQUM7QUFDTCxDQUFDLENBQUM7QUFvQk8sc0JBQUs7QUFsQmQsTUFBTSxJQUFJLEdBQUcsS0FBSyxFQUFFLEtBQXNCLEVBQXdCLEVBQUU7SUFDbEUsTUFBTSxTQUFTLEdBQUcsS0FBSyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztJQUUvRSxJQUFJLENBQUMsU0FBUyxFQUFFO1FBQ2QsT0FBTyxJQUFBLGtCQUFRLEVBQUMsR0FBRyxFQUFFO1lBQ25CLE1BQU0sRUFBRSxPQUFPO1lBQ2YsT0FBTyxFQUFFLDhCQUE4QjtTQUN4QyxDQUFDLENBQUM7S0FDSjtJQUVELE1BQU0sR0FBRyxHQUFHLE1BQU0sSUFBQSxXQUFNLEVBQUMsRUFBRSxHQUFHLEVBQUUsU0FBUyxFQUFFLENBQUMsQ0FBQztJQUU3QyxPQUFPLElBQUEsa0JBQVEsRUFBQyxHQUFHLEVBQUU7UUFDbkIsTUFBTSxFQUFFLFNBQVM7UUFDakIsR0FBRztLQUNKLENBQUMsQ0FBQztBQUNMLENBQUMsQ0FBQztBQUVjLG9CQUFJIn0=