"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getVod = exports.listVods = void 0;
const aws_sdk_1 = require("aws-sdk");
const s3 = new aws_sdk_1.S3({ apiVersion: '2006-03-01' });
;
const listVods = async () => {
    const vods = await s3.listObjects({ Bucket: 'unified-sessions' }).promise();
    return vods.Contents;
};
exports.listVods = listVods;
const getVod = async (params) => {
    const { key } = params;
    const vod = await s3.getSignedUrlPromise('getObject', {
        Bucket: 'unified-sessions',
        Key: key,
        Expires: 60 * 60, // 1 hour
    });
    return vod;
};
exports.getVod = getVod;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiczMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi9zcmMvczMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQUEscUNBQTZCO0FBRTdCLE1BQU0sRUFBRSxHQUFHLElBQUksWUFBRSxDQUFDLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBRSxDQUFDLENBQUM7QUFJL0MsQ0FBQztBQUVGLE1BQU0sUUFBUSxHQUFHLEtBQUssSUFBSSxFQUFFO0lBQzFCLE1BQU0sSUFBSSxHQUFHLE1BQU0sRUFBRSxDQUFDLFdBQVcsQ0FBQyxFQUFFLE1BQU0sRUFBRSxrQkFBa0IsRUFBRSxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUM7SUFFNUUsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDO0FBQ3ZCLENBQUMsQ0FBQztBQWVBLDRCQUFRO0FBYlYsTUFBTSxNQUFNLEdBQUcsS0FBSyxFQUFFLE1BQXNCLEVBQUUsRUFBRTtJQUM5QyxNQUFNLEVBQUUsR0FBRyxFQUFFLEdBQUcsTUFBTSxDQUFDO0lBRXZCLE1BQU0sR0FBRyxHQUFHLE1BQU0sRUFBRSxDQUFDLG1CQUFtQixDQUFDLFdBQVcsRUFBRTtRQUNwRCxNQUFNLEVBQUUsa0JBQWtCO1FBQzFCLEdBQUcsRUFBRSxHQUFHO1FBQ1IsT0FBTyxFQUFFLEVBQUUsR0FBRyxFQUFFLEVBQUUsU0FBUztLQUM1QixDQUFDLENBQUM7SUFFSCxPQUFPLEdBQUcsQ0FBQztBQUNiLENBQUMsQ0FBQztBQUlBLHdCQUFNIn0=