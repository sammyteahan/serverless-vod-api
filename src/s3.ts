import { S3 } from 'aws-sdk';

const s3 = new S3({ apiVersion: '2006-03-01' });

interface GetObjectParms {
  key: string,
};

const listVods = async () => {
  const vods = await s3.listObjects({ Bucket: 'unified-sessions' }).promise();

  return vods.Contents;
};

const getVod = async (params: GetObjectParms) => {
  const { key } = params;

  const vod = await s3.getSignedUrlPromise('getObject', {
    Bucket: 'unified-sessions',
    Key: key,
    Expires: 60 * 60, // 1 hour
  });

  return vod;
};

export {
  listVods,
  getVod,
};
