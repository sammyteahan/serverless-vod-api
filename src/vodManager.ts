import { APIGatewayEvent } from 'aws-lambda';

import { listVods, getVod } from './s3';
import response, { APIResponse } from './response';

const index = async (): Promise<APIResponse> => {
  const vods = await listVods();

  return response(200, {
    status: 'success',
    vods: vods,
  });
};

const show = async (event: APIGatewayEvent): Promise<APIResponse> => {
  const objectKey = event.pathParameters ? event.pathParameters.objectKey : null;

  if (!objectKey) {
    return response(400, {
      status: 'error',
      message: 'Please include an object key',
    });
  }

  const vod = await getVod({ key: objectKey });

  return response(200, {
    status: 'success',
    vod,
  });
};

export { index, show };
