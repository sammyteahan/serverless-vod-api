export interface APIResponse {
  headers: {
    [key: string]: string
  }
  statusCode: number
  body: string
}

function response(statusCode: number, body: any): APIResponse {
  return {
    headers: { 'Access-Control-Allow-Origin': '*' },
    statusCode,
    body: JSON.stringify(body),
  };
}

export { response as default };
